<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Services\Artwork;


/**
 * @brief Artwork Controller
 * @details Simply return the resulting RSS feed as a JSON array ready for any local builds to digest
 * 
 */
class ArtworkController extends Controller
{

	//feed uri to use
	const feedUri = 'http://backend.deviantart.com/rss.xml?q=gallery%3Apotroast';
	//artwork collection array
	private $artWorkCollection;
    
    /**
     * @brief List the artwork provided as a json array
     * @return [json]
     */
	public function listAction(Artwork $artworkServiceInstance) {


		$artworkServiceInstance->setUri(self::feedUri);
		$this->setArtworkCollection($artworkServiceInstance->listArtwork());

		return response()->json($artworkServiceInstance->getFeed());

	}

	/**
	 * @brief Set the artwwork collection
	 */
	private function setArtworkCollection($artWorkCollection) {
		$this->artWorkCollection = $artWorkCollection;
		return $this;
	}

	/**
	 * @brief Return the artwork collection array
	 * @return [array]
	 */
	private function getArtworkCollection() {
		return $this->artworkCollection;
	}
 
}
