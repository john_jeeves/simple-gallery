<?php

namespace App\Http\Middleware;

use Closure;

/**
 * @brief Ensure that the result allows external website access
 * @details [long description]
 * 
 */
class Cors {
    public function handle($request, Closure $next)
    {
        return $next($request)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
}
