<?php
namespace App\Library\Services;

class Artwork {

	/**
	 * set a key name for memcache to use
	 */
	const keyName = 'artwork_cache';


	private $uri;
	private $feed;
	private $memcached;
	private $cache;

	/**
	 * @brief Either return the artwork from any available cache or digest the external RSS feed and then process it via the cleanArtwork method
	 */
	private function getArtwork() {

		if (!empty($this->getCache())) {
			$feed = unserialize($this->getCache());
		} else {
			$options = [
				'http' => [
					'method' => 'GET',
					'header' => implode("\n", ['Accept-Encoding: gzip,deflate', 'User-Agent: Default']),
				],
			];
			$stream = stream_context_create($options);
			$feed = file_get_contents($this->getUri(), false, $stream);
			$this->setCache($feed);
		}

		$this->setFeed(simplexml_load_string($feed));

		return $this;

	}

	/**
	 * @brief Return a simpler array then the object returned via the processing of the RSS feed, this is to allow for less processor grief
	 */
	private function cleanFeed() {

		$collection = $this->getArtwork()->getFeed();
		$result = [];

		if (!empty($collection)) {

			$collection = $collection->channel;
			$count = 0;

			foreach ($collection->item as $v) {

				//deviantart exposes the larger quality images via the RSS media attributes
				$media = $v->children('media', true)->content;
				$media = current($media->attributes()->url);

				$result[$count]['title'] = current($v->title);
				$result[$count]['media'] = $media;
				$result[$count]['link'] = current($v->link);

				$count++;

			}

		}

		$this->setFeed($result);

		return $this;

	}

	/**
	 * @brief Expose the list action so that any feed results that are available can be used
	 */
	public function listArtwork() {

		$results = [];

		if (!empty($this->getUri())) {

			$this->cleanFeed();
			$results = $this->getFeed();

		}

		return $results;

	}

	/**
	 * @brief Set the feed uri to use
	 */
	public function setUri($uri) {
		$this->uri = $uri;
		return $this;
	}

	/**
	 * @brief Get the feed uri to use
	 */
	public function getUri() {
		return $this->uri;
	}


	/**
	 * @brief set the memcahed connection
	 */
	public function setMemcached(\Memcached $memcached) {
		$this->memcached = $memcached;
		return $this;
	}

	/**
	 * @brief Get the memcache connection or setup if not available
	 */
	public function getMemCached() {
		if (empty($this->memcached)) {
			$this->setMemcached(new \Memcached);
			$this->memcached->addServer('127.0.0.1', 11211);
		}
		return $this->memcached;
	}


	
	/**
	 * @brief Set the cache string

	 */
	public function setCache($string) {
		$this->getMemCached()->set(self::keyName, serialize($string), time() + 300);
		$this->cache = $this->getMemCached()->get(self::keyName);
		return $this;
	}

	/**
	 * @brief Get the cache string
	 */
	public function getCache() {
		$this->cache = $this->getMemCached()->get(self::keyName);
		return $this->cache;
	}

	/**
	 * Set the feed contents string
	 */
	private function setFeed($feed) {
		$this->feed = $feed;
		return $this;
	}

	/**
	 * @brief get the feed string
	 */
	public function getFeed() {
		return $this->feed;
	}

	

}