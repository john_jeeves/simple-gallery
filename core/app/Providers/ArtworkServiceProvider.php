<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\Artwork;

class ArtworkServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services and return the Artwork service
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\Artwork', function ($app) {
          return new Artwork();
        });
    }
}
