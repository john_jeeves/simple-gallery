Simple Gallery
-----------------
@author John Jeeves <johnnyvibrant@gmail.com>
@date 23/01/2018
@version 1.0


Introduction
---------------
This simple gallery poroject exists to demonstrate certain key skills:

* A basic Laravel installation to act as a simple Core system provider
* The use of a service provider and accompaning service to digest an external RSS feed from Deviant art of paintings I have created.
* The rate limiting and caching of feed results to ensure that some basic respect is given to the external feed provider.
* The resulting RSS feed is simplified and then returned as simple JSON array for the local builds to digest.
* A simple local front end website build that simply digests the Core results and displays them in a attractive manner.



Core
-----
The Core system is a basic Laravel installation that enables Cors via middleware to allow external websites access to the resulting JSON array and also requires that Memcached is installed on the server to store the results for five minutes to allow for basic scaling.

Files updated:

* /core/routes/web.php
* /core/app/Http/Controllers/ArtworkController.php
* /core/app/Http/Middleware/Cors.php
* /core/app/Http/Kernel.php
* /core/app/Library/Services/Artwork.php
* /core/app/Providers/ArtworkServiceProvider.php

Local
------
The local build is just front end based and uses the modern fetch api to digest the Core result JSON so as such requires a modern browser (not IE11). This then uses Masonry.js to layout out the paintings in a attractive manner.

