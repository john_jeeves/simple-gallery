/**
 * Fetch the JSON feed from the Core installation and the build the display
 */


var feedUri = 'http://localhost:8000/artwork',
    container = document.querySelector('main.main');


fetch(feedUri)
    .then((response) => response.json())
    .then((responseJson) => {

        var gallery = responseJson;

        if (_.isEmpty(gallery)) {

            container.innerHTML = '<h3>Sorry gallery failed to load</h3>';

        } else {

            var totalPictures = _.size(gallery);

            container.innerHTML = '';

            _.forEach(gallery, function(picture, i) {

                //each item
                var item = document.createElement('figure'),
                    img = document.createElement('img'),
                    title = document.createElement('figcaption');

                img.src = picture.media;
                img.setAttribute('alt', picture.title);

                title.innerHTML = picture.title;

                item.appendChild(img);
                item.appendChild(title);

                item.classList.add('grid-item');

                container.appendChild(item);

                //When all the images have been retrieved wait until the last one is actually loaded and then enable masonry to stop any layout issues
                if (i === (totalPictures - 1)) {
                    img.onload = function() {

                        var msnry = new Masonry(container, {
                            itemSelector: '.grid-item'
                        });



                    }
                }

            });





        }





    })
    .catch((error) => {
        console.error(error);
    });